#!/usr/bin/python

import os
import gtk

def youtube_click(self):
  os.system("python webview.py http://m.youtube.com ios false &")

def facebook_click(self):
  os.system("python webview.py http://facebook.com desktop true &")

def instagram_click(self):
  os.system("python webview.py http://m.instagram.com android true &")

def wicd_click(self):
  os.system("wicd &")

def full_screen(self):
  win.fullscreen()

win=gtk.Window()
hbox=gtk.HBox()
win.add(hbox)

youtube=gtk.Button()
youtube.set_label("YouTube")
youtube.connect("clicked",youtube_click)

facebook=gtk.Button()
facebook.set_label("Facebook")
facebook.connect("clicked",facebook_click)

instagram=gtk.Button()
instagram.set_label("Instagram")
instagram.connect("clicked",instagram_click)

wicd=gtk.Button()
wicd.set_label("Connect wifi")
wicd.connect("clicked",wicd_click)

hbox.pack_start(youtube)
hbox.pack_start(facebook)
hbox.pack_start(instagram)
hbox.pack_start(wicd)

win.show_all()
win.connect("destroy", lambda x: gtk.main_quit())
win.connect('window-state-event',full_screen)
win.resize(800, 600)
win.maximize()
win.fullscreen()
gtk.main()

